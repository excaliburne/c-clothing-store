import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAYScCkujY2UKr8EpAZG8Ep2XObjf3HrwI",
    authDomain: "crwn-db-4b279.firebaseapp.com",
    databaseURL: "https://crwn-db-4b279.firebaseio.com",
    projectId: "crwn-db-4b279",
    storageBucket: "crwn-db-4b279.appspot.com",
    messagingSenderId: "297743065350",
    appId: "1:297743065350:web:b15c2394ae007f5ad13e87"
  };

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`)
  const snapShot = await userRef.get()

  if (!snapShot.exists) {
    const { displayName, email } = userAuth
    const createdAt = new Date()

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log("Error creating user, ", error.message)
    }
  }

  return userRef
}

firebase.initializeApp(config)

export const auth = firebase.auth()
export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({ prompt: 'select_account' })
export const signInWithGoogle = () => auth.signInWithPopup(provider)

export default firebase
